# Controller for OF switches 'snake' performance tests
## Motivation
To prove that SWOS outperforms other software switches such as DPDK-Accelerated OVS we should 
justify and describe the reference pipelines.
Pipeline format should be well-known and simple to reproduce by 3rt parties so we use ryu app for that purpose.

## Physical topology and paths
![Testbed Diagram](img/diagram.png "Testbed Diagram")

* DUT - Device under test (OF switch)
* Transport ports - internal ports looped back to the switch (let us reproduce real pipeline)
* Access ports are connected to Traffic Generator

## Install Ryu
```
apt -y update
apt -y install git python-pip
pip install ryu

```

## Run Controller Ryu App

```
ryu-manager --verbose ryu_xsnake_vlan_controller.py
```

## Install Mininet (optional)
You need mininet to test ryu scripts without switch. If you use real switch you don't need mininet.
First clone Kirill Varlamov's fork of mininet (until his PR https://github.com/mininet/mininet/pull/751 merged to main branch)
This fork allows creating links between the ports of the same switch. Master branch doesn't allow this currently.

```
git clone -b add_links_loopback_patchcord https://github.com/ongrid/mininet.git
cd mininet/
util/install.sh
```

## Run Mininet (optional)
Run virtual switch for Snake (in case if you have no real hardware to test your own pipeline)

```
python mininet_xsnake_topo.py
```