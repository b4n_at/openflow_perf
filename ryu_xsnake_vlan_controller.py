#!/usr/bin/python
#from ryu.cmd import manager
# -*- coding: utf-8 -*-
from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.ofproto.ofproto_v1_2 import OFPG_ANY
from ryu.ofproto.ofproto_v1_3 import OFP_VERSION


class RApp(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    def __init__(self, *args, **kwargs):
        super(RApp, self).__init__(*args, **kwargs)
        self.datapath = None
        self.ofproto = None
        self.parser = None

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def switch_features_handler(self, ev):
        self.datapath = ev.msg.datapath
        self.ofproto = self.datapath.ofproto
        self.parser = self.datapath.ofproto_parser
        for i in range(0, 4):
            self.remove_flows(i)
        for [inport, outport] in [[1, 2], [2, 1], [3, 4], [4, 3]]:
            for vlan in range(1, 4001):
                match = self.parser.OFPMatch(in_port=inport, vlan_vid=(0x1000 | vlan))
                actions = [self.parser.OFPActionOutput(outport)]
                inst = [self.parser.OFPInstructionActions(self.ofproto.OFPIT_APPLY_ACTIONS, actions)]
                mod = self.parser.OFPFlowMod(datapath=self.datapath,
                                             match=match, instructions=inst)
                self.datapath.send_msg(mod)

    def remove_flows(self, table_id):
        """Removing all flow entries."""
        empty_match = self.parser.OFPMatch()
        instructions = []
        ofproto = self.datapath.ofproto
        flow_mod = self.datapath.ofproto_parser.OFPFlowMod(self.datapath, 0, 0, table_id,
                                                           ofproto.OFPFC_DELETE, 0, 0,
                                                           1,
                                                           ofproto.OFPCML_NO_BUFFER,
                                                           ofproto.OFPP_ANY,
                                                           OFPG_ANY, 0,
                                                           empty_match, instructions)
        print "deleting all flow entries in table ", table_id
        self.datapath.send_msg(flow_mod)

#manager.main(args=['--verbose', '--ofp-tcp-listen-port', '6666', RApp().__module__])
