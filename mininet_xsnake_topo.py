#!/usr/bin/python

"""
This is a simple example that demonstrates multiple links
between nodes.
"""

from mininet.cli import CLI
from mininet.node import RemoteController
from mininet.log import setLogLevel
from mininet.net import Mininet
from mininet.topo import Topo

def runXSnake():
    "Create and run multiple link network"
    topo = XSnakeTopo()
    net = Mininet( topo=topo, controller=lambda name: RemoteController( name, defaultIP='127.0.0.1' ))
    net.start()
    CLI( net )
    net.stop()

class XSnakeTopo( Topo ):
    "Simple topology with multiple links"

    def __init__( self, **kwargs ):
        Topo.__init__( self, **kwargs )
        h1, h2 = self.addHost( 'h1' ), self.addHost( 'h2' )
        s1 = self.addSwitch( 's1' )
        self.addLink( s1, h1 )
        self.addLink( s1, h2 )
        self.addLink( s1, s1 )

if __name__ == '__main__':
    setLogLevel( 'info' )
    runXSnake()
